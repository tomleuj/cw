/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arithmetical_operators.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 14:14:48 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/09 16:41:01 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		add(t_inst inst, t_vm *unused, t_proc *proc)
{
	intmax_t p[2];
	intmax_t ret;

	ft_bzero(&p, sizeof(p));
	ret = 0;
	(void)unused;
	ft_memmove((char*)&p[0], proc->reg[(inst.param_values[0] - 1)], REG_SIZE);
	ft_memmove((char*)&p[1], proc->reg[(inst.param_values[1] - 1)], REG_SIZE);
	ret = p[0] + p[1];
	ft_memmove(proc->reg[inst.param_values[2] - 1], (char*)&ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}

int		sub(t_inst inst, t_vm *unused, t_proc *proc)
{
	intmax_t p[2];
	intmax_t ret;

	ft_bzero(&p, sizeof(p));
	ret = 0;
	(void)unused;
	ft_memmove((char*)&p[0], proc->reg[(inst.param_values[0] - 1)], REG_SIZE);
	ft_memmove((char*)&p[1], proc->reg[(inst.param_values[1] - 1)], REG_SIZE);
	ret = p[0] - p[1];
	ft_memmove(proc->reg[inst.param_values[2] - 1], (char*)&ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}
