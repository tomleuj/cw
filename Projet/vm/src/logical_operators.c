/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logical_operators.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 17:29:05 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/09 17:31:46 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		set_param(t_inst inst, t_vm *vm, t_proc *proc, intmax_t *param)
{
	int		i;

	i = 0;
	while (i < 2)
	{
		if (inst.param_types[i] == REG_CODE)
			ft_memmove((char*)&param[i], proc->reg[(inst.param_values[i] - 1)],
					REG_SIZE);
		else if (inst.param_types[i] == IND_CODE)
		{
			memsplit_read((char*)&param[i], vm->mem + inst.ind_values[i],
					vm->mem, REG_SIZE);
			param[i] = endian(&param[i], REG_SIZE);
		}
		else
			ft_memmove((char*)&param[i], (char*)&inst.param_values[i],
					inst.param_sizes[i]);
		i++;
	}
}

int				and(t_inst inst, t_vm *vm, t_proc *proc)
{
	intmax_t	param[2];
	intmax_t	ret;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	ret = param[0] & param[1];
	ft_memmove(proc->reg[(inst.param_values[2] - 1)], (char*)&ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}

int				or(t_inst inst, t_vm *vm, t_proc *proc)
{
	intmax_t	param[2];
	intmax_t	ret;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	ret = param[0] | param[1];
	ft_memmove(proc->reg[(inst.param_values[2] - 1)], (char*)&ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}

int				xor(t_inst inst, t_vm *vm, t_proc *proc)
{
	intmax_t	param[2];
	intmax_t	ret;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	ret = param[0] ^ param[1];
	ft_memmove(proc->reg[(inst.param_values[2] - 1)], (char*)&ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}
