/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_vm.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 16:11:05 by lperret           #+#    #+#             */
/*   Updated: 2017/05/11 15:10:35 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static int	is_pc(int i, t_proc *proc)
{
	while (proc)
	{
		if (proc->pc == (uintmax_t)i)
			return (1);
		proc = proc->prev;
	}
	return (0);
}

void		choose_color(int color)
{
	if (color == -1)
		ft_printf(RESET);
	else if (color == 0)
		ft_printf(TUR);
	else if (color == 1)
		ft_printf(GRE);
	else if (color == 2)
		ft_printf(YEL);
	else if (color == 3)
		ft_printf(BLU);
	else if (color == 4)
		ft_printf(PIN);
	else if (color == 5)
		ft_printf(RED);
	else if (color == 6)
		ft_printf(WHI);
}

void		print_dump(t_vm vm)
{
	intmax_t i;

	i = 0;
	ft_printf(WHI);
	ft_printf(RESET);
	while (i < MEM_SIZE)
	{
		if (i == 0)
			ft_printf("0x%#.4lx (%4li) :          ", i, i);
		if (is_pc(i, vm.end_proc))
			ft_printf(WHI_B);
		choose_color(vm.color[i]);
		ft_printf("%.2x", (unsigned char)vm.mem[i]);
		ft_printf(RESET);
		if (i + 1 < MEM_SIZE && (i + 1) % (BYTES_PER_LINE / 2) == 0)
			ft_printf("\n%#.4lx (%4li) :          ", i + 1, i + 1);
		else
			ft_printf(" ");
		i++;
	}
	ft_printf("\n");
	exit(1);
}

void		print_begin(t_vm vm)
{
	int i;

	i = 0;
	ft_printf("Introducing contestants...\n");
	while (i < vm.nb_players)
	{
		ft_printf("Player %li, weighing %u bytes, \"%s\" \"%s\" !\n",
			vm.players[i].id, vm.players[i].prog_len,
			vm.players[i].name, vm.players[i].comment);
		i++;
	}
}

void		print_end(t_vm vm)
{
	int		i;

	i = 0;
	while (i < vm.nb_players)
	{
		if (vm.winner == vm.players[i].id)
			break ;
		i++;
	}
	if (vm.winner == -1)
	{
		i = vm.nb_players - 1;
		vm.winner = vm.players[i].id;
	}
	ft_printf("Contestants %li, \"%s\", has won !\n",
		vm.winner, vm.players[i].name);
}
