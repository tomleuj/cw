/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endian.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/06 15:14:58 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/09 16:40:20 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

uintmax_t		endian(void *addr, int n)
{
	int			i;
	intmax_t	ret;
	char		*tmp;
	char		*ptr;

	if (n < 0 || n > (int)sizeof(intmax_t))
		return (0);
	i = 0;
	ret = 0;
	ptr = (char*)&ret;
	tmp = (char*)addr;
	while (i < n)
	{
		*(ptr + n - 1 - i) = *tmp;
		tmp++;
		i++;
	}
	return (ret);
}
