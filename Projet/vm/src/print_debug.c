/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_vm.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 16:11:05 by lperret           #+#    #+#             */
/*   Updated: 2017/05/10 19:49:19 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	print_proc(t_proc *proc)
{
	int			i;
	intmax_t	tmp;

	while (proc)
	{
		i = 0;
		while (i < REG_NUMBER)
		{
			tmp = 0;
			ft_memmove(&tmp, proc->reg[i], REG_SIZE);
			ft_printf("reg[%d] : %lu   ", i + 1, tmp);
			i++;
		}
		ft_printf("pc : %4jd   ", proc->pc);
		ft_printf("\n");
		proc = proc->next;
	}
}

void	print_octet(char *s, int size)
{
	int		i;
	int		nb_byte_by_line;

	nb_byte_by_line = size;
	i = 0;
	while (i < size)
	{
		ft_printf("%.2x", (unsigned char)*(s + i));
		if (i > 0 && ((i + 1) % nb_byte_by_line == 0))
			ft_printf("\n");
		else
			ft_printf(" ");
		i++;
	}
	ft_printf("\n");
}

void	print_inst(t_proc *proc)
{
	t_inst	inst;
	int		i;

	inst = proc->inst;
	choose_color(proc->color);
	ft_printf("name inst : %s\n", inst.op->name);
	ft_printf("ocp : %#hhx\n", inst.ocp);
	i = 0;
	while (i < inst.op->nb_params)
	{
		choose_color(proc->color);
		ft_printf("param_sizes[%d] : %d\n", i, (int)inst.param_sizes[i]);
		i++;
	}
	i = 0;
	while (i < inst.op->nb_params)
	{
		choose_color(proc->color);
		ft_printf("param_values[%d] : %d\n", i, (int)inst.param_values[i]);
		i++;
	}
	ft_printf(WHI);
}

void	print_proc_addr(t_vm *vm)
{
	t_proc		*proc;

	proc = vm->end_proc;
	while (proc)
	{
		if (((long unsigned int)proc > 0xffffffffffff &&
			(long unsigned int)proc > 0) ||
			((long unsigned int)proc->prev > 0xffffffffffff &&
			(long unsigned int)proc->prev > 0) ||
			((long unsigned int)proc->next > 0xffffffffffff &&
			(long unsigned int)proc->next > 0))
		{
			ft_printf(RED);
			ft_printf("proc : %p\n", proc);
			ft_printf("proc->prev : %p\n", proc->prev);
			ft_printf("proc->next : %p\n", proc->next);
		}
		proc = proc->prev;
		ft_printf(WHI);
	}
	ft_printf("\n");
}
