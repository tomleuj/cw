/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_visual_mem.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 17:19:43 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 16:08:23 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static int			is_pc(int i, t_proc *proc)
{
	while (proc)
	{
		if (proc->pc == (uintmax_t)i)
			return (1);
		proc = proc->prev;
	}
	return (0);
}

static void			set_ixy(int *i, int *x, int *y)
{
	*i = 0;
	*x = 1;
	*y = 1;
}

void				print_mem(t_vm vm)
{
	int			pair;
	int			i;
	int			x;
	int			y;

	set_ixy(&i, &x, &y);
	while (i < MEM_SIZE)
	{
		pair = vm.color[i] + 2;
		if (is_pc(i, vm.end_proc))
			pair += 10;
		if (is_pc(i, vm.end_proc) || vm.gourmet[i] || pair == 1)
			wattron(vm.viewer.memory, A_BOLD);
		wattron(vm.viewer.memory, COLOR_PAIR(pair));
		mvwprintw(vm.viewer.memory, y, x, "%.2x",
				(unsigned char)vm.mem[i]);
		wattroff(vm.viewer.memory, COLOR_PAIR(pair));
		if (vm.gourmet[i] || is_pc(i, vm.end_proc) || pair == 1)
			wattroff(vm.viewer.memory, A_BOLD);
		if (i > 0 && ((i + 1) % BYTES_PER_LINE == 0))
			wmove(vm.viewer.memory, ++y, x);
		x = (i > 0 && ((i + 1) % BYTES_PER_LINE == 0)) ? 1 : x + 3;
		i++;
	}
	wrefresh(vm.viewer.memory);
}
