/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_inst.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/25 15:46:10 by lperret           #+#    #+#             */
/*   Updated: 2017/05/11 11:50:50 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static int				find_param_types(int i, t_inst inst)
{
	int		tmp;

	if (!inst.op->has_pcode)
	{
		tmp = inst.op->param_types[0];
		if (tmp == T_REG)
			return (REG_CODE);
		else if (tmp == T_DIR)
			return (DIR_CODE);
		else if (tmp == T_IND)
			return (IND_CODE);
		else
			return (0);
	}
	else
		return ((inst.ocp >> (6 - (2 * i))) & 0x03);
}

static int				find_param_types_t(int i, t_inst inst)
{
	if (inst.param_types[i] == REG_CODE)
		return (T_REG);
	else if (inst.param_types[i] == DIR_CODE)
		return (T_DIR);
	else if (inst.param_types[i] == IND_CODE)
		return (T_IND);
	else
		return (0);
}

static int				find_param_sizes(char param_type, int has_idx)
{
	if (param_type == REG_CODE)
		return (T_REG);
	else if (param_type == DIR_CODE)
		return ((has_idx) ? T_IND : T_DIR);
	else if (param_type == IND_CODE)
		return (T_IND);
	else
		return (0);
}

static uintmax_t		find_param_values(int i, t_vm *vm, t_proc *proc,
		t_inst *inst)
{
	uintmax_t	uret;
	intmax_t	sret;
	char		*ptr;
	uintmax_t	tmp;

	uret = 0;
	ptr = NULL;
	ptr = vm->mem + proc->pc + inst->size_inst + 1;
	memsplit_read(&uret, ptr, vm->mem, inst->param_sizes[i]);
	uret = endian(&uret, inst->param_sizes[i]);
	sret = cast(uret, inst->param_sizes[i]);
	if (inst->param_types[i] == IND_CODE)
	{
		if (inst->op->opcode > 12)
			tmp = (proc->pc + uret) % MEM_SIZE;
		else
			tmp = (uintmax_t)(proc->pc + sret % IDX_MOD) % MEM_SIZE;
		inst->ind_values[i] = tmp;
		ptr = vm->mem + tmp;
		uret = 0;
		memsplit_read(&uret, ptr, vm->mem, REG_SIZE);
		uret = endian(&uret, REG_SIZE);
	}
	return (uret);
}

void					set_inst(t_vm *vm, t_proc *proc)
{
	int		i;

	i = 0;
	if (proc->inst.op->has_pcode)
	{
		proc->inst.ocp = vm->mem[(proc->pc + 1) % MEM_SIZE];
		proc->inst.size_inst++;
	}
	while (i < proc->inst.op->nb_params)
	{
		proc->inst.param_types[i] = find_param_types(i, proc->inst);
		proc->inst.param_types_t[i] = find_param_types_t(i, proc->inst);
		proc->inst.param_sizes[i] = find_param_sizes(proc->inst.param_types[i],
				proc->inst.op->has_idx);
		proc->inst.param_values[i] = find_param_values(i, vm, proc,
				&proc->inst);
		proc->inst.size_inst += proc->inst.param_sizes[i];
		i++;
	}
	proc->inst.size_inst++;
}
