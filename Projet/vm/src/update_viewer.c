/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_viewer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 15:33:35 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/10 16:04:54 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		quit_corewar(t_vm *vm)
{
	free(vm->viewer.memory);
	free(vm->viewer.info);
	endwin();
	exit(1);
}

static void		pause_mode(t_vm *vm)
{
	int key;

	key = -1;
	while (vm->viewer.pause)
	{
		while (key != ' ' && key != 'n' && key != 'q')
		{
			key = getch();
			print_info(*vm);
			print_mem(*vm);
		}
		if (key == ' ')
			vm->viewer.pause = 0;
		else if (key == 'n')
			break ;
		else if (key == 'q')
			quit_corewar(vm);
	}
}

static void		speed_mode(t_vm *vm, int key)
{
	if (key == KEY_UP && vm->viewer.speed < 10)
		vm->viewer.speed += 1;
	if (key == KEY_DOWN && vm->viewer.speed > 0)
		vm->viewer.speed -= 1;
}

static void		get_keyboard_event(t_vm *vm, int key)
{
	if (key == ' ')
	{
		vm->viewer.pause = 1;
		print_info(*vm);
	}
	pause_mode(vm);
	speed_mode(vm, key);
	if (key == 'q')
		quit_corewar(vm);
}

void			update_viewer(t_vm *vm)
{
	int key;
	int i;

	key = getch();
	get_keyboard_event(vm, key);
	i = 1000 - vm->viewer.speed * 100;
	print_mem(*vm);
	print_info(*vm);
	while (i--)
		usleep(100);
}
