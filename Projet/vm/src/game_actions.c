/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_actions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 16:43:32 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 17:55:15 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

t_proc		*destroy_one_proc(t_vm *vm, t_proc *proc)
{
	t_proc		*tmp;

	tmp = proc;
	if (!proc->prev)
	{
		if (!proc->next)
			vm->end_proc = NULL;
		else
			proc->next->prev = NULL;
		proc = NULL;
	}
	else
	{
		if (!proc->next)
			vm->end_proc = proc->prev;
		else
			proc->next->prev = proc->prev;
		proc->prev->next = (!proc->next) ? NULL : proc->next;
		proc = proc->prev;
	}
	free(tmp);
	vm->nb_proc--;
	return (proc);
}

void		destroy_all_proc(t_vm *vm, t_proc *proc)
{
	t_proc		*tmp;

	while (proc)
	{
		tmp = proc->prev;
		ft_memdel((void**)&proc);
		proc = tmp;
	}
	vm->nb_proc = 0;
}

void		decrease_gourmet(char *gourmet)
{
	int		i;

	i = 0;
	while (i < MEM_SIZE)
	{
		if (gourmet[i])
			gourmet[i]--;
		i++;
	}
}
