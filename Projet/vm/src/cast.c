/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cast.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 12:45:04 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 16:39:58 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

intmax_t	cast(uintmax_t nb, int nb_octet)
{
	int			i;
	uintmax_t	max;
	char		*ptr;

	if (!(nb_octet > 0 && nb_octet <= (int)sizeof(uintmax_t)))
		return (0);
	max = 0;
	ptr = (char*)&max;
	i = 0;
	while (i < nb_octet)
	{
		*ptr++ = 0xFF;
		i++;
	}
	if (nb_octet < (int)sizeof(uintmax_t))
		nb = nb % (max + 1);
	if (nb > (max / 2))
		return (nb - max - 1);
	else
		return ((intmax_t)nb);
}
