/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_verbose.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 15:38:08 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/10 19:49:50 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void		normiblast_verbose_inst(t_vm *vm, t_proc *proc, int size_total)
{
	int	i;

	i = 0;
	while (i < proc->inst.op->nb_params)
	{
		if (proc->inst.param_types[i] == REG_CODE)
			ft_printf(" r%ju(%i)", proc->inst.param_values[i],
					*(int *)proc->reg[proc->inst.param_values[i] - 1]);
		else if (proc->inst.param_types[i] == IND_CODE &&
				!(proc->inst.op->opcode == 2 || proc->inst.op->opcode == 10
				|| proc->inst.op->opcode == 13 || proc->inst.op->opcode == 14))
		{
			ft_printf(" %jd", cast(endian(vm->mem +
							(proc->pc + size_total) % MEM_SIZE, 2), 2));
			ft_printf("(IDX_MODE=%jd)", (proc->inst.ind_values[i] - proc->pc));
		}
		else if (proc->inst.param_types[i] == IND_CODE)
			ft_printf("  %jd", cast(proc->inst.param_values[i], REG_SIZE));
		else
			ft_printf("  %jd", cast(proc->inst.param_values[i],
						proc->inst.param_sizes[i]));
		size_total += proc->inst.param_sizes[i];
		i++;
	}
}

void		print_verbose_inst(t_vm *vm, t_proc *proc)
{
	int		nb_proc;
	int		size_total;
	t_proc	*tmp;

	tmp = proc;
	nb_proc = 0;
	size_total = 1;
	(void)vm;
	if (proc->inst.op->has_pcode)
		size_total++;
	while (tmp)
	{
		tmp = tmp->prev;
		nb_proc++;
	}
	ft_printf("P %i | %s", nb_proc, proc->inst.op->name);
	normiblast_verbose_inst(vm, proc, size_total);
	ft_printf("\n");
}

void		print_move(t_vm *vm, t_proc *proc, uintmax_t pc_before)
{
	uintmax_t diff;
	uintmax_t i;

	i = 0;
	if (proc->pc > pc_before)
		diff = proc->pc - pc_before;
	else
	{
		diff = MEM_SIZE - pc_before;
		diff += proc->pc;
	}
	ft_printf("ADV %ji (%#.4jx -> %#.4jx) ", diff, pc_before, proc->pc);
	while (pc_before < MEM_SIZE && diff)
	{
		ft_printf("%.2hhx ", vm->mem[pc_before++]);
		diff--;
	}
	while (i < proc->pc && diff)
	{
		ft_printf("%.2x ", vm->mem[i++]);
		diff--;
	}
	ft_printf("\n");
}
