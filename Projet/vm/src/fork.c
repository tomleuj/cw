/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/20 15:46:19 by lperret           #+#    #+#             */
/*   Updated: 2017/04/28 16:26:46 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		furk(t_inst inst, t_vm *vm, t_proc *proc)
{
	t_proc		*new_proc;
	intmax_t	index;
	intmax_t	ret;

	index = cast(inst.param_values[0], inst.param_sizes[0]);
	ret = index % IDX_MOD;
	ret += (intmax_t)proc->pc;
	if (!(new_proc = ft_memalloc(sizeof(t_proc))))
		return (0);
	ft_memmove(new_proc, proc, sizeof(t_proc));
	new_proc->pc = (uintmax_t)ret % MEM_SIZE;
	new_proc->prev = vm->end_proc;
	vm->end_proc->next = new_proc;
	vm->end_proc = new_proc;
	new_proc->next = NULL;
	vm->nb_proc++;
	return (1);
}

int		lfurk(t_inst inst, t_vm *vm, t_proc *proc)
{
	t_proc		*new_proc;
	intmax_t	index;
	intmax_t	ret;

	index = cast(inst.param_values[0], inst.param_sizes[0]);
	ret = index;
	ret += (intmax_t)proc->pc;
	if (!(new_proc = ft_memalloc(sizeof(t_proc))))
		return (0);
	ft_memmove(new_proc, proc, sizeof(t_proc));
	new_proc->pc = (uintmax_t)ret % MEM_SIZE;
	new_proc->prev = vm->end_proc;
	vm->end_proc->next = new_proc;
	vm->end_proc = new_proc;
	new_proc->next = NULL;
	vm->nb_proc++;
	return (1);
}
