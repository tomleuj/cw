/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   live.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 11:11:46 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/16 12:59:27 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		live(t_inst inst, t_vm *vm, t_proc *proc)
{
	int		current_player;

	vm->nbr_live++;
	proc->alive = 1;
	current_player = 0;
	while (current_player < vm->nb_players)
	{
		if ((intmax_t)inst.param_values[0] == vm->players[current_player].id)
		{
			vm->winner = vm->players[current_player].id;
			if (!vm->visual)
				ft_printf("un processus dit que le joueur %jd(%s) est en vie\n",
					vm->players[current_player].id,
					vm->players[current_player].name);
				/*ft_printf("LE JOUEUR %s (%jd) EST EN VIE !\n",
					vm->players[current_player].name,
					vm->players[current_player].id);*/
			return (1);
		}
		current_player++;
	}
	if (!vm->visual)
	{
		//ft_printf("INSTRUCTION (LIVE) NON VALIDE: %ji", inst.param_values[0]);
		ft_printf("instruction (live) non valide: %ji", inst.param_values[0]);
		ft_printf(" n'est pas l'id d'un joueur.\n");
	}
	return (1);
}
