/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_players.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 16:27:59 by lperret           #+#    #+#             */
/*   Updated: 2017/05/11 12:37:56 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static int		set_player_name(t_player *player, char *cor, int i)
{
	int j;

	j = 0;
	while (j < PROG_NAME_LENGTH + 1)
	{
		ft_memmove(player->name + j, cor + i, 4);
		j += 4;
		i += 4;
	}
	return (i);
}

static int		set_player_comment(t_player *player, char *cor, int i)
{
	int j;

	j = 0;
	while (j < COMMENT_LENGTH + 1)
	{
		ft_memmove(player->comment + j, cor + i, 4);
		j += 4;
		i += 4;
	}
	return (i);
}

static void		set_player(t_player *player, char *av, int fd)
{
	unsigned int	*prog_len;
	unsigned int	i;
	char			*cor;

	i = 4;
	cor = readin(fd, player);
	if (player->size < sizeof(t_header))
		print_size_error(av, cor);
	i = set_player_name(player, cor, i);
	prog_len = (unsigned int *)(cor + i);
	player->prog_len = endian(prog_len, sizeof(*prog_len));
	if (endian(&cor, sizeof(int) != COREWAR_EXEC_MAGIC) || player->prog_len !=
		player->size - sizeof(t_header) || player->prog_len > CHAMP_MAX_SIZE)
		print_header_error(av, cor);
	i += 4;
	i = set_player_comment(player, cor, i);
	ft_memmove(player->instruct, cor + i, player->prog_len);
	free(cor);
}

static int		is_available_id(t_vm *vm)
{
	int i;
	int id_valid;

	i = 0;
	id_valid = 1;
	while (i < vm->nb_players + 1)
	{
		if (vm->players[i].id == id_valid)
		{
			id_valid++;
			i = -1;
		}
		i++;
	}
	return (id_valid);
}

void			set_players(t_vm *vm, char *av, int nb, int n_mode)
{
	int		fd;

	if (vm->nb_players >= MAX_PLAYERS)
		print_too_many_champs_error();
	if ((fd = open(av, O_RDONLY)) == -1)
		print_file_error(av);
	set_player(&vm->players[vm->nb_players], av, fd);
	if (n_mode)
		vm->players[vm->nb_players].id = nb;
	else
		vm->players[vm->nb_players].id = is_available_id(vm);
	vm->nb_players++;
	vm->nb_proc = (uintmax_t)vm->nb_players;
	close(fd);
}
