/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_proc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 17:01:43 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 17:49:14 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		print_malloc_error(t_proc *top)
{
	t_proc		*tmp;

	while (top)
	{
		tmp = top->next;
		free(top);
		top = tmp;
	}
	ft_putendl_fd("error : malloc error", 2);
	exit(1);
}

static t_proc	*getlst(t_proc *lst)
{
	t_proc	*elem;

	if (!(elem = (t_proc *)ft_memalloc(sizeof(*elem))))
		return (NULL);
	if (!lst)
		lst = elem;
	else
	{
		lst->next = elem;
		elem->prev = lst;
	}
	return (elem);
}

static void		set_process(t_proc *lst, uintmax_t space,
													int num_player, t_vm vm)
{
	t_proc		*tmp;

	tmp = lst;
	while (tmp)
	{
		ft_memmove(tmp->reg[0], &vm.players[num_player].id, REG_SIZE);
		tmp = tmp->next;
	}
	lst->pc = space * num_player;
	lst->color = num_player % 7;
	lst->alive = 1;
}

t_proc			*set_proc(t_vm vm)
{
	int			num_player;
	uintmax_t	space;
	t_proc		*lst;
	t_proc		*top;

	num_player = 0;
	lst = NULL;
	top = NULL;
	space = MEM_SIZE / vm.nb_players;
	while (num_player < vm.nb_players)
	{
		if (!(lst = getlst(lst)))
			print_malloc_error(top);
		if (!top)
			top = lst;
		set_process(lst, space, num_player, vm);
		num_player++;
	}
	return (top);
}
