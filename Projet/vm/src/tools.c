/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 12:40:46 by tlejeune          #+#    #+#             */
/*   Updated: 2017/05/10 18:58:07 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	memsplit_read(void *dst, void *src, char *mem, size_t size)
{
	char		*dst_ptr;
	char		*src_ptr;
	size_t		diff;
	size_t		i;

	dst_ptr = (char*)dst;
	src_ptr = (char*)src;
	i = 0;
	while (i < size)
	{
		diff = (src_ptr + i) - mem;
		diff = diff % MEM_SIZE;
		ft_memmove(dst_ptr + i, mem + diff, 1);
		i++;
	}
}

void	memsplit_write(void *dst, void *src, char *mem, size_t size)
{
	char		*dst_ptr;
	char		*src_ptr;
	size_t		diff;
	size_t		i;

	dst_ptr = (char*)dst;
	src_ptr = (char*)src;
	i = 0;
	while (i < size)
	{
		diff = (dst_ptr + i) - mem;
		diff = diff % MEM_SIZE;
		ft_memmove(mem + diff, src_ptr + i, 1);
		i++;
	}
}

void	memsplit_set(void *dst, int color, char *color_mem, size_t size)
{
	char		*dst_ptr;
	size_t		diff;
	size_t		i;

	dst_ptr = (char*)dst;
	i = 0;
	while (i < size)
	{
		diff = (dst_ptr + i) - color_mem;
		diff = diff % MEM_SIZE;
		ft_memset(color_mem + diff, color, 1);
		i++;
	}
}

t_op	*singleton(void)
{
	static t_op	opt[17] = {A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q};

	return (opt);
}
