/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_vm.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 15:39:28 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/09 16:47:01 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

t_vm	set_vm(void)
{
	t_vm	vm;

	ft_bzero(&vm, sizeof(t_vm));
	vm.tab_func[0] = &live;
	vm.tab_func[1] = &ld;
	vm.tab_func[2] = &st;
	vm.tab_func[3] = &add;
	vm.tab_func[4] = &sub;
	vm.tab_func[5] = &and;
	vm.tab_func[6] = &or;
	vm.tab_func[7] = &xor;
	vm.tab_func[8] = &zjmp;
	vm.tab_func[9] = &ldi;
	vm.tab_func[10] = &sti;
	vm.tab_func[11] = &furk;
	vm.tab_func[12] = &lld;
	vm.tab_func[13] = &lldi;
	vm.tab_func[14] = &lfurk;
	vm.tab_func[15] = &aff;
	vm.cycle_pool = CYCLE_TO_DIE;
	ft_memset(vm.color, -1, MEM_SIZE);
	vm.winner = -1;
	vm.dump = -1;
	return (vm);
}
