/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_opcode_and_cycle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 17:22:07 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 17:22:29 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

t_inst	get_opcode_and_cycle(t_vm *vm, t_proc *proc)
{
	t_inst	inst;

	ft_bzero(&inst, sizeof(t_inst));
	if (!((vm->mem[proc->pc]) >= 1 && ((vm->mem[proc->pc]) <= 16)))
	{
		inst.opcode_error = 1;
		return (inst);
	}
	inst.op = singleton() + ((vm->mem[proc->pc]) - 1);
	proc->cycle = inst.op->nb_cycles;
	return (inst);
}
