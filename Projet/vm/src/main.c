/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 13:38:04 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 15:35:51 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		visual_priority(t_vm *vm)
{
	if (vm->visual)
	{
		vm->verbose = 0;
		vm->dump = -1;
	}
}

static int		usage_dump(int ac, char **av, int nb, int i)
{
	if (i + 2 >= ac || !get_check_int(av[i + 1], &nb))
		print_usage_error();
	return (nb);
}

void			set_options_players(t_vm *vm, int ac, char **av)
{
	int		i;
	int		nb;

	i = 0;
	while (++i < ac)
		if (!ft_strcmp("-visual", av[i]))
			vm->visual = 1;
		else if (!ft_strcmp("-verbose", av[i]))
			vm->verbose = 1;
		else if (!ft_strcmp("-dump", av[i]))
			vm->dump = usage_dump(ac, av, nb, i++);
		else if (!ft_strcmp("-n", av[i]))
		{
			if (i + 2 >= ac || !get_check_int(av[i + 1], &nb))
				print_usage_error();
			set_players(vm, av[i + 2], nb, 1);
			i += 2;
		}
		else
			set_players(vm, av[i], 1, 0);
	visual_priority(vm);
}

void			check_define(void)
{
	if (IND_SIZE > sizeof(uintmax_t) || IND_SIZE < 1 ||
		REG_SIZE > sizeof(uintmax_t) || REG_SIZE < 1 ||
		DIR_SIZE > sizeof(uintmax_t) || DIR_SIZE < 1 ||
		T_REG != 1 || T_DIR != 4 || T_IND != 2 || T_LAB != 8 ||
		MAX_PLAYERS > 6)
	{
		ft_putendl_fd("Error : define not valid", 2);
		exit(1);
	}
}

int				main(int ac, char **av)
{
	t_proc	*top_proc;
	t_vm	vm;

	if (ac == 1)
		print_usage_error();
	check_define();
	vm = set_vm();
	set_options_players(&vm, ac, av);
	check_champs_size(vm);
	if (vm.nb_players == 0)
		print_usage_error();
	top_proc = set_proc(vm);
	load_champion(&vm, top_proc);
	if (vm.visual)
		set_visual(&vm);
	else
		print_begin(vm);
	game(&vm, top_proc);
	if (vm.visual)
		printw_end(vm);
	else
		print_end(vm);
	return (0);
}
