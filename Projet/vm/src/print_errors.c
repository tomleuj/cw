/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_errors.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 11:23:54 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 16:40:17 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	print_too_many_champs_error(void)
{
	ft_putstr_fd("Too many players : > ", 2);
	ft_putnbr_fd(MAX_PLAYERS, 2);
	ft_putendl_fd("", 2);
	exit(1);
}

void	print_usage_error(void)
{
	ft_putstr_fd("usage : corewar [-visual] [-verbose]", 2);
	ft_putendl_fd(" [-dump nbr_cycles] [[-n number] champion1.cor] ...", 2);
	ft_putendl_fd("     -visual           : Ncurses output mode", 2);
	ft_putendl_fd("     -verbose          : Verbose mode", 2);
	ft_putstr_fd("     -dump nbr_cycles  : Dumps memory after nbr_cycles", 2);
	ft_putendl_fd(" cycles (> 0) then exits", 2);
	ft_putendl_fd("     -n    number      : Attribute next player number", 2);
	exit(1);
}

void	print_size_error(char *file, char *read)
{
	ft_putstr_fd("error: file ", 2);
	ft_putstr_fd(file, 2);
	ft_putendl_fd(" is too small to be a champion", 2);
	free(read);
	exit(1);
}

void	print_header_error(char *file, char *read)
{
	ft_putstr_fd("error: file ", 2);
	ft_putstr_fd(file, 2);
	ft_putendl_fd(" has an invalid header", 2);
	free(read);
	exit(1);
}

void	print_file_error(char *file)
{
	ft_putstr_fd("error: can't read source file ", 2);
	ft_putendl_fd(file, 2);
	exit(1);
}
