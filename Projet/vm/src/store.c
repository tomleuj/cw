/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   store.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 17:40:06 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 16:46:17 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		set_param(t_inst inst, t_vm *vm, t_proc *proc, uintmax_t *param)
{
	int		i;

	i = 0;
	while (i < 2)
	{
		if (inst.param_types[i + 1] == REG_CODE)
			ft_memmove(&param[i], proc->reg[(inst.param_values[i + 1] - 1)],
					REG_SIZE);
		else if (inst.param_types[i + 1] == IND_CODE)
		{
			memsplit_read(&param[i], vm->mem + inst.ind_values[i + 1], vm->mem,
					REG_SIZE);
			param[i] = endian(&param[i], REG_SIZE);
		}
		else
			ft_memmove(&param[i], &inst.param_values[i + 1],
					inst.param_sizes[i + 1]);
		i++;
	}
}

int				st(t_inst inst, t_vm *vm, t_proc *proc)
{
	uintmax_t		tmp;

	if (inst.param_types[1] == REG_CODE)
		ft_memmove(proc->reg[(inst.param_values[1] - 1)],
				proc->reg[(inst.param_values[0] - 1)], REG_SIZE);
	else
	{
		tmp = 0;
		ft_memmove(&tmp, proc->reg[(inst.param_values[0] - 1)], REG_SIZE);
		tmp = endian(&tmp, REG_SIZE);
		memsplit_write(vm->mem + inst.ind_values[1], &tmp, vm->mem, REG_SIZE);
		memsplit_set(vm->color + inst.ind_values[1], proc->color, vm->color,
				REG_SIZE);
		memsplit_set(vm->gourmet + inst.ind_values[1], NB_CYCLE_BOLD,
				vm->gourmet, REG_SIZE);
	}
	return (1);
}

int				sti(t_inst inst, t_vm *vm, t_proc *proc)
{
	uintmax_t	param[2];
	intmax_t	ret;
	intmax_t	sum;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	ft_memmove(&ret, proc->reg[inst.param_values[0] - 1], REG_SIZE);
	ret = endian(&ret, REG_SIZE);
	sum = 0;
	sum += cast(param[0], (inst.param_types[1] == REG_CODE ||
			inst.param_types[1] == IND_CODE) ? REG_SIZE : inst.param_sizes[1]);
	sum += cast(param[1], (inst.param_types[2] == REG_CODE ||
			inst.param_types[2] == IND_CODE) ? REG_SIZE : inst.param_sizes[2]);
	memsplit_write(vm->mem + (uintmax_t)(proc->pc + sum % IDX_MOD) % MEM_SIZE,
			&ret, vm->mem, REG_SIZE);
	memsplit_set(vm->color + (uintmax_t)(proc->pc + sum % IDX_MOD) % MEM_SIZE,
			proc->color, vm->color, REG_SIZE);
	memsplit_set(vm->gourmet + (uintmax_t)(proc->pc + sum % IDX_MOD) % MEM_SIZE,
			NB_CYCLE_BOLD, vm->gourmet, REG_SIZE);
	return (1);
}
