/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_visual.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 13:38:04 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 16:11:44 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		init_color_pairs(void)
{
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_BLUE, COLOR_BLACK);
	init_pair(4, COLOR_RED, COLOR_BLACK);
	init_pair(5, COLOR_CYAN, COLOR_BLACK);
	init_pair(6, COLOR_YELLOW, COLOR_BLACK);
	init_pair(7, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(11, COLOR_WHITE, COLOR_BLACK);
	init_pair(12, COLOR_WHITE, COLOR_GREEN);
	init_pair(13, COLOR_WHITE, COLOR_BLUE);
	init_pair(14, COLOR_WHITE, COLOR_RED);
	init_pair(15, COLOR_WHITE, COLOR_CYAN);
	init_pair(16, COLOR_WHITE, COLOR_YELLOW);
	init_pair(17, COLOR_WHITE, COLOR_MAGENTA);
}

WINDOW			*create_newwin(int startx, int starty, int height, int width)
{
	WINDOW		*local_win;

	local_win = newwin(height, width, starty, startx);
	wrefresh(local_win);
	return (local_win);
}

void			set_visual(t_vm *vm)
{
	t_viewer	viewer;

	initscr();
	cbreak();
	curs_set(0);
	keypad(stdscr, TRUE);
	viewer.pause = 1;
	viewer.speed = 5;
	nodelay(stdscr, 1);
	noecho();
	refresh();
	viewer.memory = create_newwin(1, 1, MEM_LINES + 2, MEM_COLS + 2);
	viewer.info = create_newwin(MEM_COLS + 15, MEM_LINES / 5, 70, 70);
	wrefresh(viewer.info);
	wrefresh(viewer.memory);
	start_color();
	init_color_pairs();
	vm->viewer = viewer;
	print_mem(*vm);
	print_info(*vm);
}
