/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_inst.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/25 15:48:42 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 16:41:28 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static int		check_ocp(t_inst inst)
{
	int		i;

	i = 0;
	while (i < inst.op->nb_params)
	{
		if (inst.param_types_t[i] == 0)
			return (0);
		if ((inst.param_types_t[i] & inst.op->param_types[i]) !=
			inst.param_types_t[i])
			return (0);
		i++;
	}
	return (1);
}

static int		check_valid_reg(t_inst inst)
{
	int		i;

	i = 0;
	while (i < inst.op->nb_params)
	{
		if (inst.param_types[i] == REG_CODE)
			if (inst.param_values[i] < 1 || inst.param_values[i] > REG_NUMBER)
				return (0);
		i++;
	}
	return (1);
}

int				check_exe(t_vm *vm, t_proc *proc)
{
	if (!check_ocp(proc->inst))
		return (0);
	if (!check_valid_reg(proc->inst))
		return (0);
	if (vm->verbose)
		print_verbose_inst(vm, proc);
	return (1);
}
