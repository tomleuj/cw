/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_visual_info.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 17:19:02 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 16:17:51 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void			print_the_end_info(t_vm vm, int y)
{
	mvwprintw(vm.viewer.info, y++, 1, "Speed : %13d ", (vm.viewer.speed));
	y = y + 2;
	mvwprintw(vm.viewer.info, y++, 1, "Processes : %9ju", vm.nb_proc);
	mvwprintw(vm.viewer.info, y++, 1, "Current cycle : %5ju", vm.current_cycle);
	y = y + 1;
	mvwprintw(vm.viewer.info, y++, 1, "Total cycles : %6ju", vm.total_cycle);
	y = y + 1;
	mvwprintw(vm.viewer.info, y++, 1, "CYCLE_TO_DIE : %6d", vm.cycle_pool);
	mvwprintw(vm.viewer.info, y++, 1, "CYCLE_DELTA : %7d", CYCLE_DELTA);
	mvwprintw(vm.viewer.info, y++, 1, "NBR_LIVE : %10d", NBR_LIVE);
	mvwprintw(vm.viewer.info, y++, 1, "MAX_CHECKS : %8d", MAX_CHECKS);
}

void				print_info(t_vm vm)
{
	int i;
	int y;

	i = 0;
	y = 1;
	wattron(vm.viewer.info, A_BOLD);
	if (vm.viewer.pause)
		mvwprintw(vm.viewer.info, y++, 1, "        **PAUSE**");
	else
		mvwprintw(vm.viewer.info, y++, 1, "        **GAME!**");
	y = y + 4;
	while (i < vm.nb_players)
	{
		wattron(vm.viewer.info, A_BOLD);
		mvwprintw(vm.viewer.info, y++, 1, "Player %d :", vm.players[i].id);
		wattroff(vm.viewer.info, A_BOLD);
		wattron(vm.viewer.info, COLOR_PAIR(i + 2));
		mvwprintw(vm.viewer.info, y++, 1, "%.65s", vm.players[i].name);
		wattroff(vm.viewer.info, COLOR_PAIR(i++ + 2));
		y = y + 2;
	}
	wattron(vm.viewer.info, A_BOLD);
	print_the_end_info(vm, y);
	wattroff(vm.viewer.info, A_BOLD);
	wrefresh(vm.viewer.info);
}

void				printw_end(t_vm vm)
{
	int		i;
	int		key;

	WINDOW * w_winner;
	i = 0;
	key = 0;
	w_winner = create_newwin(MEM_COLS + 15, MEM_LINES + 1, 20, 50);
	while (i < vm.nb_players)
		if (vm.winner == vm.players[i].id)
			break ;
		else
			i++;
	if (vm.winner == -1)
	{
		i = vm.nb_players - 1;
		vm.winner = vm.players[i].id;
	}
	wattron(w_winner, A_BOLD);
	mvwprintw(w_winner, 0, 0, "Contestant %li, \"%s\", has won !\n",
		vm.winner, vm.players[i].name);
	wrefresh(w_winner);
	wattroff(w_winner, A_BOLD);
	while (key != 'q')
		key = getch();
	endwin();
}
