/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_champion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 16:59:54 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 17:46:56 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void	load_champion(t_vm *vm, t_proc *proc)
{
	unsigned int	n;
	int				num_player;
	int				memsize_by_player;

	ft_bzero(vm->mem, MEM_SIZE);
	num_player = 0;
	memsize_by_player = MEM_SIZE / vm->nb_players;
	while (proc)
	{
		n = 0;
		while (n < vm->players[num_player].prog_len)
		{
			vm->mem[n + proc->pc] = vm->players[num_player].instruct[n];
			vm->color[n + proc->pc] = proc->color;
			n++;
		}
		proc = proc->next;
		num_player++;
	}
}
