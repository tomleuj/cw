/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/24 12:19:55 by lperret           #+#    #+#             */
/*   Updated: 2017/05/10 19:47:07 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		aff(t_inst inst, t_vm *vm, t_proc *proc)
{
	char	tmp;

	if (!vm->visual)
	{
		tmp = 0;
		tmp = proc->reg[inst.param_values[0] - 1][0];
		ft_printf("%c", tmp);
	}
	return (1);
}
