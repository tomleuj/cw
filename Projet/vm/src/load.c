/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ld.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/17 16:55:55 by lperret           #+#    #+#             */
/*   Updated: 2017/05/09 17:38:24 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int				ld(t_inst inst, t_vm *unused, t_proc *proc)
{
	(void)unused;
	ft_memmove(proc->reg[(inst.param_values[1] - 1)],
			&inst.param_values[0],
			REG_SIZE);
	proc->carry = (inst.param_values[0]) ? 0 : 1;
	return (1);
}

int				lld(t_inst inst, t_vm *unused, t_proc *proc)
{
	return (ld(inst, unused, proc));
}

static void		set_param(t_inst inst, t_vm *vm, t_proc *proc, uintmax_t *param)
{
	int		i;

	i = 0;
	while (i < 2)
	{
		if (inst.param_types[i] == REG_CODE)
			ft_memmove(&param[i], proc->reg[(inst.param_values[i] - 1)],
					REG_SIZE);
		else if (inst.param_types[i] == IND_CODE)
		{
			memsplit_read(&param[i], vm->mem + inst.ind_values[i], vm->mem,
					REG_SIZE);
			param[i] = endian(&param[i], REG_SIZE);
		}
		else
			ft_memmove(&param[i], &inst.param_values[i], inst.param_sizes[i]);
		i++;
	}
}

int				ldi(t_inst inst, t_vm *vm, t_proc *proc)
{
	uintmax_t	param[2];
	intmax_t	ret;
	intmax_t	sum;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	sum = 0;
	sum += cast(param[0], (inst.param_types[0] == REG_CODE ||
			inst.param_types[0] == IND_CODE) ? REG_SIZE : inst.param_sizes[0]);
	sum += cast(param[1], (inst.param_types[1] == REG_CODE ||
			inst.param_types[1] == IND_CODE) ? REG_SIZE : inst.param_sizes[1]);
	memsplit_read(&ret, vm->mem +
		((uintmax_t)(proc->pc + sum % IDX_MOD) % MEM_SIZE), vm->mem, REG_SIZE);
	ret = endian(&ret, REG_SIZE);
	ft_memmove(proc->reg[(inst.param_values[2] - 1)], &ret, REG_SIZE);
	return (1);
}

int				lldi(t_inst inst, t_vm *vm, t_proc *proc)
{
	uintmax_t	param[2];
	intmax_t	ret;
	intmax_t	sum;

	ft_bzero(&param, sizeof(param));
	ret = 0;
	set_param(inst, vm, proc, param);
	sum = 0;
	sum += cast(param[0], (inst.param_types[0] == REG_CODE ||
			inst.param_types[0] == IND_CODE) ? REG_SIZE : inst.param_sizes[0]);
	sum += cast(param[1], (inst.param_types[1] == REG_CODE ||
			inst.param_types[1] == IND_CODE) ? REG_SIZE : inst.param_sizes[1]);
	memsplit_read(&ret, vm->mem +
		((uintmax_t)(proc->pc + sum) % MEM_SIZE), vm->mem, REG_SIZE);
	ret = endian(&ret, REG_SIZE);
	ft_memmove(proc->reg[(inst.param_values[2] - 1)], &ret, REG_SIZE);
	proc->carry = (ret) ? 0 : 1;
	return (1);
}
