/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/11 18:38:08 by lperret           #+#    #+#             */
/*   Updated: 2017/05/11 14:54:50 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		treat_inst(t_vm *vm, t_proc *proc, uintmax_t pc_before)
{
	set_inst(vm, proc);
	if (check_exe(vm, proc))
		vm->tab_func[proc->inst.op->opcode - 1](proc->inst, vm, proc);
	if (proc->inst.op->opcode != 9)
	{
		proc->pc = (proc->pc + proc->inst.size_inst) % MEM_SIZE;
		if (vm->verbose)
			print_move(vm, proc, pc_before);
	}
}

static int		read_proc(t_vm *vm)
{
	t_proc		*proc;
	uintmax_t	pc_before;

	proc = vm->end_proc;
	while (proc)
	{
		pc_before = proc->pc;
		if (!proc->cycle)
			proc->inst = get_opcode_and_cycle(vm, proc);
		if (proc->inst.opcode_error)
		{
			proc->pc = (proc->pc + 1) % MEM_SIZE;
			proc->cycle = 0;
		}
		else
		{
			proc->cycle--;
			if (!proc->cycle)
				treat_inst(vm, proc, pc_before);
		}
		proc = proc->prev;
	}
	return (1);
}

static void		newturn(t_vm *vm)
{
	t_proc		*proc;

	proc = vm->end_proc;
	while (proc)
	{
		if (!proc->alive)
			proc = destroy_one_proc(vm, proc);
		else
		{
			proc->alive = 0;
			proc = proc->prev;
		}
	}
	vm->current_cycle = 0;
	if (vm->nbr_live < NBR_LIVE)
		vm->nbr_check++;
	if (vm->nbr_live >= NBR_LIVE || vm->nbr_check == MAX_CHECKS)
	{
		vm->cycle_pool -= CYCLE_DELTA;
		vm->nbr_check = 0;
	}
	if (vm->cycle_pool < 0)
		vm->cycle_pool = 0;
	vm->nbr_live = 0;
}

int				game(t_vm *vm, t_proc *top_proc)
{
	vm->end_proc = top_proc;
	while (vm->end_proc->next)
		vm->end_proc = vm->end_proc->next;
	while (vm->nb_proc)
	{
		newturn(vm);
		while (vm->current_cycle < vm->cycle_pool && vm->nb_proc)
		{
			if (vm->dump == vm->total_cycle)
				print_dump(*vm);
			vm->current_cycle++;
			vm->total_cycle++;
			if (vm->verbose)
				ft_printf("It is now cycle %ji\n", vm->total_cycle);
			read_proc(vm);
			decrease_gourmet(vm->gourmet);
			if (vm->visual)
				update_viewer(vm);
		}
	}
	if (vm->visual)
		update_viewer(vm);
	return (1);
}
