/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zjmp.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 16:00:52 by lperret           #+#    #+#             */
/*   Updated: 2017/05/10 19:51:21 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

int		zjmp(t_inst inst, t_vm *vm, t_proc *proc)
{
	intmax_t	ret;
	intmax_t	index;
	uintmax_t	pc_before;

	index = cast(inst.param_values[0], inst.param_sizes[0]);
	pc_before = proc->pc;
	if (proc->carry)
	{
		ret = (index % IDX_MOD);
		ret += (intmax_t)proc->pc;
		proc->pc = (uintmax_t)ret;
	}
	else
		proc->pc += inst.size_inst;
	proc->pc %= MEM_SIZE;
	if (vm->verbose && !proc->carry)
	{
		ft_printf("zjmp FAILED\n");
		print_move(vm, proc, pc_before);
	}
	return (1);
}
