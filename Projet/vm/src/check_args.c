/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/25 15:48:42 by lperret           #+#    #+#             */
/*   Updated: 2017/05/10 20:04:27 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

void			check_champs_size(t_vm vm)
{
	unsigned int	size_max_by_player;
	int				i;

	size_max_by_player = MEM_SIZE / vm.nb_players;
	i = 0;
	while (i < vm.nb_players)
	{
		if (vm.players[i].prog_len > size_max_by_player)
		{
			ft_putstr_fd("Error : champion ", 2);
			ft_putstr_fd(vm.players[i].name, 2);
			ft_putstr_fd(" number(", 2);
			ft_putnbr_fd(i, 2);
			ft_putstr_fd(") is too fat\n", 2);
			exit(1);
		}
		i++;
	}
}

static int		neg_check_atoi_itoa(char *itoa, char *av)
{
	char	*ret;

	ret = NULL;
	av++;
	while (*av == '0' && *(av + 1))
		av++;
	if (*av == '0')
		return (1);
	if (!(ret = ft_strjoin("-", av)))
		return (0);
	if (ft_strcmp(ret, itoa))
	{
		free(ret);
		return (0);
	}
	free(ret);
	return (1);
}

static int		check_atoi_itoa(char **itoa, char *av)
{
	if (!(*itoa = ft_itoa(ft_atoi(av))))
		return (0);
	if (*av == '-')
	{
		if (!neg_check_atoi_itoa(*itoa, av))
			return (0);
	}
	else
	{
		while (*av == '0' && *(av + 1))
			av++;
		if (ft_strcmp(av, *itoa))
			return (0);
	}
	return (1);
}

int				get_check_int(char *av, int *nb)
{
	char *itoa;

	*nb = -1;
	itoa = NULL;
	if (!check_atoi_itoa(&itoa, av))
	{
		free(itoa);
		return (0);
	}
	*nb = ft_atoi(av);
	free(itoa);
	return (1);
}
