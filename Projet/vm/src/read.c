/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 19:29:41 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/11 12:46:10 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vm.h"

static void		print_malloc_error(void)
{
	ft_putendl_fd("error : malloc error", 2);
	exit(1);
}

static char		*enlarge_str(char *ret, char buffer, size_t pos)
{
	char	*tmp;

	tmp = ret;
	if (!(ret = (char*)ft_memalloc(pos + 1)))
	{
		free(tmp);
		return (NULL);
	}
	if (tmp)
		ft_memmove(ret, tmp, pos);
	ret[pos] = buffer;
	free(tmp);
	return (ret);
}

char			*readin(int fd, t_player *player)
{
	size_t		pos;
	char		*ret;
	char		buffer;

	pos = 0;
	ret = NULL;
	while (read(fd, &buffer, 1) > 0)
	{
		if (!(ret = enlarge_str(ret, buffer, pos)))
			print_malloc_error();
		pos++;
	}
	player->size = pos;
	return (ret);
}
