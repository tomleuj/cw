/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 13:38:58 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/16 12:34:48 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VM_H
# define VM_H
# include <ncurses.h>
# include "ft_printf.h"
# include "op.h"

# define MEM_UNIT			3
# define BYTES_PER_LINE 	64
# define MEM_COLS			MEM_UNIT * BYTES_PER_LINE
# define MEM_LINES			MEM_SIZE / BYTES_PER_LINE
# define INFO_COLS			55
# define INFO_LINES 		MEM_LINES
# define NB_CYCLE_BOLD		25

# define A					{"live", 1, {4}, 1, 10, 0, 0}
# define B					{"ld", 2, {6, 1}, 2, 5, 1, 0}
# define C					{"st", 2, {1, 3}, 3, 5, 1, 0}
# define D					{"add", 3, {1, 1, 1}, 4, 10, 1, 0}
# define E					{"sub", 3, {1, 1, 1}, 5, 10, 1, 0}
# define F					{"and", 3, {7, 7, 1}, 6, 6, 1, 0}
# define G					{"or", 3, {7, 7, 1}, 7, 6, 1, 0}
# define H					{"xor", 3, {7, 7, 1}, 8, 6, 1, 0}
# define I					{"zjmp", 1, {4}, 9, 20, 0, 1}
# define J					{"ldi", 3, {7, 5, 1}, 10, 25, 1, 1}
# define K					{"sti", 3, {1, 7, 5}, 11, 25, 1, 1}
# define L					{"fork", 1, {4}, 12, 800, 0, 1}
# define M					{"lld", 2, {6, 1}, 13, 10, 1, 0}
# define N					{"lldi", 3, {7, 5, 1}, 14, 50, 1, 1}
# define O					{"lfork", 1, {4}, 15, 1000, 0, 1}
# define P					{"aff", 1, {1}, 16, 2, 1, 0}
# define Q					{0, 0, {0}, 0, 0, 0, 0}

typedef struct				s_op
{
	char					*name;
	int						nb_params;
	char					param_types[4];
	char					opcode;
	int						nb_cycles;
	int						has_pcode;
	int						has_idx;
}							t_op;

typedef struct				s_inst
{
	t_op					*op;
	char					ocp;
	int						opcode_error;
	char					param_types[3];
	char					param_types_t[3];
	int						param_sizes[3];
	uintmax_t				param_values[3];
	uintmax_t				ind_values[3];
	int						size_inst;
}							t_inst;

typedef struct				s_proc
{
	char					reg[REG_NUMBER][REG_SIZE];
	uintmax_t				pc;
	int						cycle;
	int						carry;
	int						color;
	t_inst					inst;
	int						alive;
	struct s_proc			*prev;
	struct s_proc			*next;
}							t_proc;

typedef struct				s_player
{
	unsigned int			size;
	char					name[PROG_NAME_LENGTH + 1];
	char					comment[COMMENT_LENGTH + 1];
	char					instruct[CHAMP_MAX_SIZE];
	unsigned int			prog_len;
	intmax_t				id;
}							t_player;

typedef struct				s_viewer
{
	int						pause;
	int						speed;
	WINDOW					*memory;
	WINDOW					*info;
}							t_viewer;

typedef struct				s_vm
{
	int						visual;
	int						verbose;
	intmax_t				dump;
	t_player				players[MAX_PLAYERS];
	int						nb_players;
	uintmax_t				nb_proc;
	char					color[MEM_SIZE];
	char					gourmet[MEM_SIZE];
	char					mem[MEM_SIZE];
	int						(*tab_func[16])(t_inst, struct s_vm *, t_proc *);
	int						current_cycle;
	int						cycle_pool;
	intmax_t				total_cycle;
	intmax_t				winner;
	int						nbr_live;
	int						nbr_check;
	t_proc					*end_proc;
	t_viewer				viewer;
}							t_vm;

char						*readin(int fd, t_player *player);
void						set_players(t_vm *vm, char *av, int nb,
								int n_mode);
void						check_champs_size(t_vm vm);
t_proc						*set_proc(t_vm vm);
t_op						*singleton(void);
intmax_t					cast(uintmax_t nb, int nb_octet);
void						memsplit_read(void *dst, void *src, char *m,
								size_t s);
void						memsplit_write(void *dst, void *src, char *m,
		size_t s);
t_proc						*set_proc(t_vm vm);
intmax_t					cast(uintmax_t nb, int nb_octet);
void						memsplit_read(void *dst, void *src, char *m,
		size_t s);
void						memsplit_write(void *dst, void *src, char *m,
		size_t s);
void						memsplit_set(void *dst, int col, char *col_m,
		size_t s);
void						load_champion(t_vm *vm, t_proc *proc);
void						choose_color(int color);
void						print_dump(t_vm vm);
void						print_begin(t_vm vm);
void						print_end(t_vm vm);
void						printw_end(t_vm vm);
void						print_move(t_vm *vm, t_proc *proc,
		uintmax_t pc_before);
void						print_verbose_inst(t_vm *vm, t_proc *proc);
void						print_octet(char *s, int size);
void						print_proc(t_proc *proc);
void						print_proc_addr(t_vm *vm);
void						print_inst(t_proc *proc);
t_inst						get_opcode_and_cycle(t_vm *vm, t_proc *proc);
void						set_inst(t_vm *vm, t_proc *proc);
int							check_exe(t_vm *vm, t_proc *proc);
int							get_check_int(char *av, int *nb);
int							live(t_inst inst, t_vm *vm, t_proc *proc);
int							add(t_inst inst, t_vm *vm, t_proc *proc);
int							sub(t_inst inst, t_vm *vm, t_proc *proc);
int							and(t_inst inst, t_vm *vm, t_proc *proc);
int							or(t_inst inst, t_vm *vm, t_proc *proc);
int							xor(t_inst inst, t_vm *vm, t_proc *proc);
int							ld(t_inst inst, t_vm *vm, t_proc *proc);
int							st(t_inst inst, t_vm *vm, t_proc *proc);
int							zjmp(t_inst inst, t_vm *vm, t_proc *proc);
int							ldi(t_inst inst, t_vm *vm, t_proc *proc);
int							sti(t_inst inst, t_vm *vm, t_proc *proc);
int							furk(t_inst inst, t_vm *vm, t_proc *proc);
int							lld(t_inst inst, t_vm *vm, t_proc *proc);
int							lldi(t_inst inst, t_vm *vm, t_proc *proc);
int							lfurk(t_inst inst, t_vm *vm, t_proc *proc);
int							aff(t_inst inst, t_vm *vm, t_proc *proc);
uintmax_t					endian(void *addr, int n);
int							check(int ac, char **av);
t_vm						set_vm(void);
int							game(t_vm *vm, t_proc *proc);
t_proc						*destroy_one_proc(t_vm *vm, t_proc *proc);
void						destroy_all_proc(t_vm *vm, t_proc *proc);
void						decrease_gourmet(char *gourmet);

WINDOW						*create_newwin(int startx, int starty,
		int height, int width);
void						set_visual(t_vm *vm);
void						update_viewer(t_vm *vm);
void						print_info(t_vm vm);
void						print_mem(t_vm vm);
void						print_too_many_champs_error(void);
void						print_usage_error(void);
void						print_size_error(char *file, char *read);
void						print_header_error(char *file, char *read);
void						print_file_error(char *file);

#endif
