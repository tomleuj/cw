/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 16:40:27 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/15 14:51:21 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H
# include <stdio.h>
# include "ft_printf.h"
# include "op.h"

# define ERROR_HEADER 				1
# define ERROR_LABEL_EXISTING 		2
# define ERROR_LABEL_CONSECUTIVE 	3
# define ERROR_INSTR 				4
# define ERROR_MALLOC 				5

# define A					{"live", 1, {4}, 1, 10, 0, 0}
# define B					{"ld", 2, {6, 1}, 2, 5, 1, 0}
# define C					{"st", 2, {1, 3}, 3, 5, 1, 0}
# define D					{"add", 3, {1, 1, 1}, 4, 10, 1, 0}
# define E					{"sub", 3, {1, 1, 1}, 5, 10, 1, 0}
# define F					{"and", 3, {7, 7, 1}, 6, 6, 1, 0}
# define G					{"or", 3, {7, 7, 1}, 7, 6, 1, 0}
# define H					{"xor", 3, {7, 7, 1}, 8, 6, 1, 0}
# define I					{"zjmp", 1, {4}, 9, 20, 0, 1}
# define J					{"ldi", 3, {7, 5, 1}, 10, 25, 1, 1}
# define K					{"sti", 3, {1, 7, 5}, 11, 25, 1, 1}
# define L					{"fork", 1, {4}, 12, 800, 0, 1}
# define M					{"lld", 2, {6, 1}, 13, 10, 1, 0}
# define N					{"lldi", 3, {7, 5, 1}, 14, 50, 1, 1}
# define O					{"lfork", 1, {4}, 15, 1000, 0, 1}
# define P					{"aff", 1, {1}, 16, 2, 1, 0}
# define Q					{0, 0, {0}, 0, 0, 0, 0}

typedef struct						s_op
{
	char							*name;
	int								nb_params;
	int								param_types[4];
	int								opcode;
	int								nb_cycles;
	int								has_pcode;
	int								has_idx;
}									t_op;

typedef struct						s_core
{
	char							*label;
	t_op							*op;
	char							**params;
	int								params_code[4];
	size_t							size;
	struct s_core					*next;
}									t_core;

int									index_first_inst(char **ali);
int									set_lst(t_core **lst, char **ali);
t_op								*singleton(void);

intmax_t							cast(uintmax_t nb, int nb_octet);
uintmax_t							endian(void *addr, int n);
t_core								*set_elem(t_core *elem, char ***split,
		char **label, int index);
void								free_lst(t_core *lst);
void								free_ppvoid(char **s);
int									check_filename(char *name);
int									error_bad_filename(void);
char								**get_champion(int fd);
int									error_empty_file(void);
void								changetabtospace_delcom(char **champion);
char								**get_labels(char **champion,
		int *num_row_error);
int									has_label(char *line);
int									has_instr(char *line);
int									is_an_existing_label(char *label,
		char **labels);
int									has_two_consecutive_labels(char **champion,
		int *num_row_error);
int									check_syntax(char **champion);
int									has_correct_header(char **champion,
		int *num_row_error);
int									is_emptyline_or_hashtagcomment(char *line);
int									is_comment(char *line);
char								**get_instr(char ***champion, char **begin,
		int *num_row);
int									check_instr(char **champion, char **labels,
		int *num_row_error);
int									check_one_param(int num_inst, int num_param,
		char *param, char **labels);
int									ppvoid_len(void **pp);
t_header							init_header(t_core *list, char **champion);
void								write_dir(int fd, t_core *list, t_core *ptr,
		char *params);
void								write_ind(int fd, t_core *list, t_core *ptr,
		char *params);
int									write_cor(t_core *list, t_header header,
		char *name);

#endif
