path_ref='../test_asm_ref/'
path='test_asm/'
path_asm='../../asm'
path_asm_ref='../../../../Binaires42/asm'

rm -rf $path/*.cor
cd $path
rm -rf $path_ref
mkdir $path_ref

for fichier in *.s
do
	cp $fichier $path_ref
	printf "\033[32m"
	printf "$fichier: " 
	$path_asm $fichier
	printf "\033[33m"
	printf "$fichier _ref: " 
	$path_asm_ref $path_ref$fichier
	printf "\033[34m"
done

for fichier in *.cor
do
	printf "$fichier: " 
	diff $fichier $path_ref$fichier
	printf "\n" 
done

printf "\033[0m"
