/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 12:40:46 by tlejeune          #+#    #+#             */
/*   Updated: 2017/05/15 17:38:12 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				index_first_inst(char **ali)
{
	int		i;

	i = 0;
	while (!(is_comment(ali[i])))
		i++;
	i++;
	while ((is_emptyline_or_hashtagcomment(ali[i])))
		i++;
	return (i);
}

t_op			*singleton(void)
{
	static t_op	opt[17] = {A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q};

	return (opt);
}

uintmax_t		endian(void *addr, int n)
{
	int			i;
	intmax_t	ret;
	char		*tmp;
	char		*ptr;

	if (n < 0 || n > (int)sizeof(intmax_t))
		return (0);
	i = 0;
	ret = 0;
	ptr = (char*)&ret;
	tmp = (char*)addr;
	while (i < n)
	{
		*(ptr + n - 1 - i) = *tmp;
		tmp++;
		i++;
	}
	return (ret);
}

intmax_t		cast(uintmax_t nb, int nb_octet)
{
	int			i;
	uintmax_t	max;
	char		*ptr;

	if (!(nb_octet > 0 && nb_octet <= (int)sizeof(uintmax_t)))
		return (0);
	max = 0;
	ptr = (char*)&max;
	i = 0;
	while (i < nb_octet)
	{
		*ptr++ = 0xFF;
		i++;
	}
	if (nb_octet < (int)sizeof(uintmax_t))
		nb = nb % (max + 1);
	if (nb > (max / 2))
		return (nb - max - 1);
	else
		return ((intmax_t)nb);
}
