/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 20:33:27 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/16 12:08:42 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static void		write_pcode(int fd, int *params_code)
{
	char	pcode;
	int		i;

	pcode = 0;
	i = 0;
	while (params_code[i])
	{
		pcode = (char)params_code[i] | pcode;
		pcode = pcode << 2;
		i++;
	}
	if (i == 1)
		pcode = pcode << 4;
	else if (i == 2)
		pcode = pcode << 2;
	write(fd, &pcode, 1);
}

static void		write_reg(int fd, char *params)
{
	char reg;

	reg = (char)ft_atoi(params + 1);
	write(fd, &reg, 1);
}

static void		write_params(int fd, t_core *list, t_core *ptr)
{
	int	i;

	i = 0;
	while (ptr->params_code[i])
	{
		if (ptr->params_code[i] == REG_CODE)
			write_reg(fd, ptr->params[i]);
		else if (ptr->params_code[i] == DIR_CODE)
			write_dir(fd, list, ptr, ptr->params[i]);
		else if (ptr->params_code[i] == IND_CODE)
			write_ind(fd, list, ptr, ptr->params[i]);
		i++;
	}
}

int				write_cor(t_core *list, t_header header, char *name)
{
	int		fd;
	t_core	*ptr;

	ptr = list;
	name[ft_strlen(name) - 1] = '\0';
	name = ft_strjoin(name, "cor");
	if ((fd = open(name, O_RDWR | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR)) == -1)
		return (0);
	write(fd, &header, sizeof(t_header));
	while (ptr)
	{
		if (ptr->op)
		{
			write(fd, &(ptr->op->opcode), 1);
			if (ptr->op->has_pcode)
				write_pcode(fd, ptr->params_code);
			write_params(fd, list, ptr);
		}
		ptr = ptr->next;
	}
	ft_putstr("Writing output program to ");
	ft_putendl(name);
	close(fd);
	free(name);
	return (1);
}
