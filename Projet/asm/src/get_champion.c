/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_champion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 17:54:42 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 15:12:44 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static char		**free_and_close_before(char **tmp, char *line, int fd)
{
	free_ppvoid(tmp);
	free(line);
	close(fd);
	return (NULL);
}

char			**get_champion(int fd)
{
	char	**map;
	char	**tmp;
	char	*line;
	int		nb_line;
	int		i;

	map = NULL;
	nb_line = 0;
	while (get_next_line(fd, &line) > 0)
	{
		nb_line++;
		tmp = map;
		if (!(map = (char**)malloc(sizeof(char*) * (nb_line + 1))))
			return (free_and_close_before(tmp, line, fd));
		i = -1;
		while (++i < nb_line - 1)
			map[i] = tmp[i];
		free(tmp);
		map[i] = line;
		map[i + 1] = NULL;
	}
	changetabtospace_delcom(map);
	close(fd);
	return (map);
}

int				error_empty_file(void)
{
	ft_putendl_fd("empty file error", 2);
	return (1);
}
