/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_instr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 11:56:18 by lperret           #+#    #+#             */
/*   Updated: 2017/05/10 20:55:14 by aparrot          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				has_instr(char *line)
{
	if (!line)
		return (0);
	if (has_label(line))
	{
		while (*line != LABEL_CHAR)
			line++;
		line++;
	}
	if (!is_emptyline_or_hashtagcomment(line))
		return (1);
	else
		return (0);
}

static char		**get_one_instr(char *line)
{
	char	**one_instr;

	one_instr = NULL;
	if (has_label(line))
	{
		while (*line != LABEL_CHAR)
			line++;
		line++;
	}
	if (!(one_instr = ft_strsplit(line, ' ')))
		return (NULL);
	return (one_instr);
}

char			**get_instr(char ***champion, char **begin, int *num_row)
{
	char	**instr;

	if (!champion)
		return (NULL);
	instr = NULL;
	while (**champion && !instr)
	{
		if (has_instr(**champion))
			if (!(instr = get_one_instr(**champion)))
			{
				*num_row = *champion - begin + 1;
				return (NULL);
			}
		(*champion)++;
	}
	*num_row = *champion - begin;
	while (**champion && is_emptyline_or_hashtagcomment(**champion))
		(*champion)++;
	return (instr);
}
