/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_tab_to_space_and_suppr_comment.c            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/31 17:17:54 by lperret           #+#    #+#             */
/*   Updated: 2017/05/16 12:01:05 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static void		change_tab_to_space_and_suppr_comment_for_one(char *l, int af)
{
	if (!l)
		return ;
	while (*l)
	{
		if (af)
		{
			if (*l == '\t')
				*l = ' ';
		}
		if (*l == COMMENT_CHAR)
		{
			*l = '\0';
			return ;
		}
		l++;
	}
}

void			changetabtospace_delcom(char **champion)
{
	int		after_header;

	if (!champion)
		return ;
	after_header = 0;
	while (*champion)
	{
		change_tab_to_space_and_suppr_comment_for_one(*champion, after_header);
		if (is_comment(*champion))
			after_header = 1;
		champion++;
	}
}
