/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setlst.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 15:43:11 by tlejeune          #+#    #+#             */
/*   Updated: 2017/05/15 17:22:22 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static char		**strclean(char *ali)
{
	int		i;
	char	*tmp;
	char	**split;

	i = 0;
	split = NULL;
	tmp = ft_strdup(ali);
	while (tmp[i])
	{
		if (tmp[i] == '\t')
			tmp[i] = ' ';
		i++;
	}
	split = ft_strsplit(tmp, ' ');
	free(tmp);
	return (split);
}

static int		arrlen(char **array)
{
	int i;

	i = 0;
	while (array[i])
		i++;
	return (i);
}

static void		updt_lst(t_core *lst, t_core *elem)
{
	t_core *tmp;

	tmp = lst;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = elem;
}

static void		get_label(char ***split, char **label)
{
	char *ptr;

	ptr = ft_strchr(*split[0], LABEL_CHAR);
	*ptr = '\0';
	*label = ft_strdup(*split[0]);
}

int				set_lst(t_core **lst, char **ali)
{
	int		i;
	int		len;
	char	*label;
	char	**split;
	t_core	*elem;

	elem = NULL;
	label = NULL;
	i = index_first_inst(ali);
	while (ali[i])
	{
		split = strclean(ali[i++]);
		len = arrlen(split);
		if (len == 1 || len == 3)
			get_label(&split, &label);
		if (len == 1 || len == 2 || len == 3)
		{
			if (!(elem = set_elem(elem, &split, &label, (len - 2))))
				return (0);
			(!*lst) ? *lst = elem : updt_lst(*lst, elem);
		}
		free_ppvoid(split);
	}
	return (1);
}
