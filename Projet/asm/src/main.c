/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 16:46:51 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/15 17:38:37 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int	freemain(t_core *lst, char **champion, int error)
{
	free_lst(lst);
	free_ppvoid(champion);
	if (error)
		perror("Error : ");
	return (error);
}

int			main(int ac, char **av)
{
	int			fd;
	char		**champion;
	t_core		*list;
	t_header	header;

	list = NULL;
	champion = NULL;
	if (ac != 2)
		return (freemain(list, champion, 0));
	if (!check_filename(av[1]))
		return (error_bad_filename());
	if ((fd = open(av[1], O_RDONLY)) == -1)
		return (freemain(list, champion, 1));
	if (!(champion = get_champion(fd)))
		return (error_empty_file());
	if (!check_syntax(champion))
		return (freemain(list, champion, 0));
	if (!set_lst(&list, champion))
		return (freemain(list, champion, 1));
	header = init_header(list, champion);
	write_cor(list, header, av[1]);
	return (freemain(list, champion, 0));
}
