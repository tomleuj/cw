/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_syntax.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 12:55:57 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 17:32:58 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int		free_before(char **labels, int type_error, int num_row_error)
{
	free_ppvoid(labels);
	if (num_row_error == -1)
	{
		ft_putendl_fd("no instr error", 2);
		return (0);
	}
	if (type_error == ERROR_MALLOC || num_row_error == 0)
	{
		ft_putendl_fd("malloc error", 2);
		return (0);
	}
	if (type_error == ERROR_HEADER)
		ft_putstr_fd("header error : ", 2);
	else if (type_error == ERROR_LABEL_EXISTING)
		ft_putstr_fd("label already declared error : ", 2);
	else if (type_error == ERROR_LABEL_CONSECUTIVE)
		ft_putstr_fd("label consecutive error : ", 2);
	else if (type_error == ERROR_INSTR)
		ft_putstr_fd("instr error : ", 2);
	ft_putstr_fd("line ", 2);
	ft_putnbr_fd(num_row_error, 2);
	ft_putchar('\n');
	return (0);
}

int				check_syntax(char **champion)
{
	char	**labels;
	int		num_row_error;

	labels = NULL;
	num_row_error = 0;
	if (!(has_correct_header(champion, &num_row_error)))
		return (free_before(labels, ERROR_HEADER, num_row_error));
	if (!(labels = get_labels(champion, &num_row_error)))
		return (free_before(labels, ERROR_LABEL_EXISTING, num_row_error));
	if (!(check_instr(champion, labels, &num_row_error)))
		return (free_before(labels, ERROR_INSTR, num_row_error));
	free_ppvoid(labels);
	return (1);
}
