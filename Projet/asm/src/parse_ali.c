/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_ali.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/31 11:24:49 by tlejeune          #+#    #+#             */
/*   Updated: 2017/05/15 17:22:39 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static t_core	*get_elem(t_core *elem, char *label)
{
	int i;

	i = 0;
	if (!(elem = (t_core *)malloc(sizeof(*elem))))
		return (NULL);
	elem->label = NULL;
	if (label)
		elem->label = ft_strdup(label);
	elem->op = NULL;
	elem->params = NULL;
	while (i < 4)
		elem->params_code[i++] = 0;
	elem->size = 0;
	elem->next = NULL;
	return (elem);
}

static void		readin(t_core *elem, t_op *op_tab, char **split, int index)
{
	int i;

	i = 0;
	while (op_tab[i].name)
	{
		if (ft_strcmp(split[index], op_tab[i].name) == 0)
			elem->op = &op_tab[i];
		i++;
	}
	elem->params = ft_strsplit(split[index + 1], SEPARATOR_CHAR);
}

static void		getsize(t_core *elem)
{
	int i;

	i = 0;
	elem->size = 1;
	elem->size += (elem->op->has_pcode == 1) ? 1 : 0;
	while (elem->params[i])
	{
		if (elem->params[i][0] == 'r')
		{
			elem->params_code[i] = REG_CODE;
			elem->size += T_REG;
		}
		else if (elem->params[i][0] == DIRECT_CHAR)
		{
			elem->params_code[i] = DIR_CODE;
			elem->size += (elem->op->has_idx == 1) ? T_IND : T_DIR;
		}
		else
		{
			elem->params_code[i] = IND_CODE;
			elem->size += T_IND;
		}
		i++;
	}
}

t_core			*set_elem(t_core *elem, char ***split, char **label, int index)
{
	if (!(elem = get_elem(elem, *label)))
	{
		free(*label);
		free_ppvoid(*split);
		return (NULL);
	}
	if (index >= 0)
	{
		readin(elem, singleton(), *split, index);
		getsize(elem);
	}
	free(*label);
	*label = NULL;
	return (elem);
}
