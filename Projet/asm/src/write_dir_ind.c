/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_dir_ind.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 20:35:30 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/16 12:08:54 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static t_core	*find_label(t_core *list, char *label, t_core *ptr, int *before)
{
	while (list)
	{
		if (list == ptr)
			*before = 1;
		if (list->label && !ft_strcmp(list->label, label))
		{
			while (list && !list->op)
				list = list->next;
			return (list);
		}
		list = list->next;
	}
	return (NULL);
}

static int		find_relativ_adress(t_core *list, t_core *ptr, char *label)
{
	int size;
	int before;

	size = 0;
	before = 0;
	list = find_label(list, label, ptr, &before);
	if (before)
	{
		while (ptr && ptr != list)
		{
			size += ptr->size;
			ptr = ptr->next;
		}
	}
	else
	{
		while (list && ptr != list)
		{
			size += list->size;
			list = list->next;
		}
		size = -size;
	}
	return (size);
}

void			write_dir(int fd, t_core *list, t_core *ptr, char *params)
{
	intmax_t	dir;
	int			len;
	char		*label;

	if ((label = ft_strchr(params, LABEL_CHAR)))
		len = find_relativ_adress(list, ptr, label + 1);
	if (ptr->op->has_idx)
	{
		if (label)
			dir = len;
		else
			dir = ft_atoi(params + 1);
		dir = cast(endian(&dir, T_IND), T_IND);
		write(fd, &dir, T_IND);
	}
	else
	{
		if (label)
			dir = len;
		else
			dir = ft_atoi(params + 1);
		dir = cast(endian(&dir, T_DIR), T_DIR);
		write(fd, &dir, T_DIR);
	}
}

void			write_ind(int fd, t_core *list, t_core *ptr, char *params)
{
	intmax_t	ind;
	int			len;
	char		*label;

	label = NULL;
	if ((label = ft_strchr(params, LABEL_CHAR)))
		len = find_relativ_adress(list, ptr, label + 1);
	if (label)
		ind = len;
	else
		ind = ft_atoi(params);
	ind = cast(endian(&ind, T_IND), T_IND);
	write(fd, &ind, T_IND);
}
