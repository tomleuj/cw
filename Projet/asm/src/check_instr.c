/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_instr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 12:39:44 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 15:11:12 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int		ft_num_instr(char *name)
{
	int		i;

	i = 0;
	while (singleton()[i].name)
	{
		if (ft_strcmp(name, singleton()[i].name) == 0)
			return (i);
		i++;
	}
	return (-1);
}

static int		check_params(int num_instr, char **params, char **labels)
{
	int		num_param;

	if (ppvoid_len((void**)params) != singleton()[num_instr].nb_params)
		return (0);
	num_param = 0;
	while (num_param < singleton()[num_instr].nb_params)
	{
		if (!(check_one_param(num_instr, num_param, params[num_param], labels)))
			return (0);
		num_param++;
	}
	return (1);
}

static int		check_one_instr(char **instr, char **labels)
{
	int		num_instr;
	char	**args;

	args = NULL;
	if (ppvoid_len((void**)instr) == 0)
		return (1);
	if (ppvoid_len((void**)instr) != 2)
		return (0);
	if ((num_instr = ft_num_instr(*instr)) == -1)
		return (0);
	if (!(args = ft_strsplit(*(instr + 1), SEPARATOR_CHAR)))
		return (0);
	if (!(check_params(num_instr, args, labels)))
	{
		free_ppvoid(args);
		return (0);
	}
	free_ppvoid(args);
	return (1);
}

static int		check_instr_loop(char **champion, char **begin,
										char **labels, int *num_row_error)
{
	int		num_row;
	char	**instr;

	num_row = 0;
	instr = NULL;
	while (*champion)
	{
		if (has_label(*champion) && !has_instr(*champion))
			champion++;
		else
		{
			if (!(instr = get_instr(&champion, begin, &num_row)))
				return (0);
			if (!(check_one_instr(instr, labels)))
			{
				free_ppvoid(instr);
				*num_row_error = num_row;
				return (0);
			}
		}
		free_ppvoid(instr);
		instr = NULL;
	}
	return (1);
}

int				check_instr(char **champion, char **labels, int *num_row_error)
{
	char	**begin;

	begin = champion;
	if (!(*champion))
		return (0);
	while (!(is_comment(*champion)))
		champion++;
	champion++;
	if (!(*champion))
	{
		*num_row_error = -1;
		return (0);
	}
	while (is_emptyline_or_hashtagcomment(*champion))
		champion++;
	if (!(*champion))
	{
		*num_row_error = -1;
		return (0);
	}
	return (check_instr_loop(champion, begin, labels, num_row_error));
}
