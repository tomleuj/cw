/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_name_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 18:15:26 by lperret           #+#    #+#             */
/*   Updated: 2017/04/10 12:58:38 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		check_filename(char *name)
{
	size_t		name_len;

	if (!name)
		return (0);
	if ((name_len = ft_strlen(name)) < 3)
		return (0);
	if (name[name_len - 2] != '.' || name[name_len - 1] != 's')
		return (0);
	return (1);
}

int		error_bad_filename(void)
{
	ft_putendl("Bad filename");
	return (1);
}
