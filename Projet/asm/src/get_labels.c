/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_labels.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 16:18:02 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 15:13:26 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				has_label(char *line)
{
	int		belong_label_chars;
	int		i;
	char	*label_chars;

	if (!line || *line == LABEL_CHAR)
		return (0);
	if (!(label_chars = ft_strdup(LABEL_CHARS)))
		return (0);
	while (*line)
	{
		belong_label_chars = 0;
		i = 0;
		while (label_chars[i])
			if (*line == label_chars[i++])
			{
				belong_label_chars = 1;
				break ;
			}
		if (!belong_label_chars)
			break ;
		line++;
	}
	free(label_chars);
	return ((*line == LABEL_CHAR ? 1 : 0));
}

static int		ft_nb_labels(char **champion)
{
	int		nb;

	if (!champion)
		return (0);
	nb = 0;
	while (*champion)
	{
		if (has_label(*champion))
			nb++;
		champion++;
	}
	return (nb);
}

static void		init_label_to_null(char **labels, int nb_labels)
{
	while (nb_labels >= 0)
		labels[nb_labels--] = NULL;
}

static char		*create_label(char *line, char **labels)
{
	char	*label;
	size_t	i;

	if (!labels)
		return (NULL);
	i = 0;
	while (line[i] != LABEL_CHAR)
		i++;
	if (!(label = ft_strnew(i)))
		return (NULL);
	ft_memmove(label, line, i);
	if (is_an_existing_label(label, labels))
	{
		ft_memdel((void**)(&label));
		return (NULL);
	}
	return (label);
}

char			**get_labels(char **champion, int *num_row_error)
{
	char	**labels;
	int		nb_labels;
	int		i;
	int		n;

	if (!champion)
		return (NULL);
	nb_labels = ft_nb_labels(champion);
	if (!(labels = (char**)malloc(sizeof(char*) * (nb_labels + 1))))
		return (NULL);
	init_label_to_null(labels, nb_labels);
	n = 0;
	i = 0;
	while (n < nb_labels)
	{
		if (has_label(champion[i]))
			if (!(labels[n++] = create_label(champion[i], labels)))
			{
				free_ppvoid(labels);
				*num_row_error = i + 1;
				return (NULL);
			}
		i++;
	}
	return (labels);
}
