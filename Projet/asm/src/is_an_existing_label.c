/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_an_existing_label.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/04 15:20:34 by lperret           #+#    #+#             */
/*   Updated: 2017/05/15 15:56:15 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				is_an_existing_label(char *label, char **labels)
{
	if (!label || !labels)
		return (0);
	while (*labels)
	{
		if (ft_strcmp(label, *labels) == 0)
			return (1);
		labels++;
	}
	return (0);
}

static int		has_two_consecutive_labels_loop(char **champion, char **begin,
													int *num_row_error, int i)
{
	while (*champion)
	{
		if (has_label(*champion))
		{
			i = 0;
			while ((*champion)[i] != LABEL_CHAR)
				i++;
			if (is_emptyline_or_hashtagcomment(*champion + i + 1))
			{
				champion++;
				if (!(*champion))
					return (0);
				while (is_emptyline_or_hashtagcomment(*champion))
					champion++;
				if (has_label(*champion))
				{
					*num_row_error = champion - begin + 1;
					return (1);
				}
			}
		}
		champion++;
	}
	return (0);
}

int				has_two_consecutive_labels(char **champion, int *num_row_error)
{
	char	**begin;

	if (!champion)
		return (0);
	begin = champion;
	return (has_two_consecutive_labels_loop(champion, begin,
													num_row_error, 0));
}
