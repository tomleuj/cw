/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   has_correct_header.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 12:58:56 by lperret           #+#    #+#             */
/*   Updated: 2017/05/16 12:06:54 by lperret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int				is_emptyline_or_hashtagcomment(char *line)
{
	if (!line)
		return (0);
	while (*line)
	{
		if (*line == COMMENT_CHAR)
			return (1);
		else if ((*line != ' ') && (*line != '\t'))
			return (0);
		line++;
	}
	return (1);
}

static int		length_of_string(char *s)
{
	int		i;

	if (!s)
		return (0);
	i = 0;
	while (s[i] && s[i] != '"')
		i++;
	return (i);
}

static int		is_name(char *line)
{
	if (!line || ft_strlen(line) < ft_strlen(NAME_CMD_STRING) + 3)
		return (0);
	if (!(ft_strncmp(line, NAME_CMD_STRING, ft_strlen(NAME_CMD_STRING)) == 0))
		return (0);
	line += ft_strlen(NAME_CMD_STRING);
	while (*line == ' ' || *line == '\t')
		line++;
	if (*line == '\0' || *line != '"')
		return (0);
	line++;
	if (!(length_of_string(line) > 0 &&
				length_of_string(line) <= PROG_NAME_LENGTH))
		return (0);
	while (*line && *line != '"')
		line++;
	if (*line != '"')
		return (0);
	line++;
	while (*line)
	{
		if (*line != ' ' && *line != '\t')
			return (0);
		line++;
	}
	return (1);
}

int				is_comment(char *line)
{
	if (!line || ft_strlen(line) < ft_strlen(COMMENT_CMD_STRING) + 3)
		return (0);
	if (!(ft_strncmp(line, COMMENT_CMD_STRING,
					ft_strlen(COMMENT_CMD_STRING)) == 0))
		return (0);
	line += ft_strlen(COMMENT_CMD_STRING);
	while (*line == ' ' || *line == '\t')
		line++;
	if (*line == '\0' || *line != '"')
		return (0);
	line++;
	if (!(length_of_string(line) <= COMMENT_LENGTH))
		return (0);
	while (*line && *line != '"')
		line++;
	if (*line != '"')
		return (0);
	line++;
	while (*line)
	{
		if (*line != ' ' && *line != '\t')
			return (0);
		line++;
	}
	return (1);
}

int				has_correct_header(char **champion, int *num_row_error)
{
	int		i;

	i = 0;
	while (champion[i] && is_emptyline_or_hashtagcomment(champion[i]))
		i++;
	if (!is_name(champion[i]))
	{
		*num_row_error = i + 1;
		return (0);
	}
	i++;
	if (!is_comment(champion[i]))
	{
		*num_row_error = i + 1;
		return (0);
	}
	return (1);
}
