/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_one_param.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 20:54:27 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/16 14:13:39 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int	ft_param_type(char *param)
{
	if (!param)
		return (0);
	if (*param == 'r')
		return (T_REG);
	else if (*param == DIRECT_CHAR)
		return (T_DIR);
	else
		return (T_IND);
}

static int	check_reg_syntax(char *param)
{
	intmax_t	num_reg;

	if (!param)
		return (0);
	if (*param != 'r')
		return (0);
	param++;
	num_reg = 0;
	if (ft_strlen(param) > 2)
		return (0);
	while (*param >= '0' && *param <= '9')
	{
		num_reg *= 10;
		num_reg += *param - '0';
		param++;
	}
	if (*param != '\0')
		return (0);
	if (!(num_reg >= 1 && num_reg <= REG_NUMBER))
		return (0);
	//if (!(num_reg >= 0 && num_reg <= 99))
	//	return (0);
	return (1);
}

static int	is_number_ok(char *s)
{
	if (!s)
		return (0);
	if (*s == '-')
		s++;
	while (ft_isdigit(*s))
		s++;
	if (*s != '\0')
		return (0);
	else
		return (1);
}

static int	check_dir_or_ind_syntax(char *param, char **labels)
{
	if (!param)
		return (0);
	if (*param == DIRECT_CHAR)
		param++;
	if (*param == LABEL_CHAR)
	{
		param++;
		return (is_an_existing_label(param, labels));
	}
	else
		return (is_number_ok(param));
	return (1);
}

int			check_one_param(int num_instr, int num_param,
											char *param, char **labels)
{
	int		param_type;

	if (!param || !labels)
		return (0);
	param_type = ft_param_type(param);
	if ((singleton()[num_instr].param_types[num_param] & param_type) !=
																param_type)
		return (0);
	if (param_type == T_REG)
		return (check_reg_syntax(param));
	else if (param_type == T_DIR || param_type == T_IND)
		return (check_dir_or_ind_syntax(param, labels));
	else
		return (0);
}
