/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   freez.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/04 16:50:46 by tlejeune          #+#    #+#             */
/*   Updated: 2017/05/15 14:48:15 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	free_lst(t_core *lst)
{
	t_core *tmp;

	while (lst)
	{
		tmp = lst->next;
		free_ppvoid(lst->params);
		free(lst->label);
		free(lst);
		lst = NULL;
		lst = tmp;
	}
}

void	free_ppvoid(char **s)
{
	char **tmp;

	tmp = NULL;
	if (s)
	{
		tmp = s;
		while (*tmp)
		{
			free(*tmp);
			*tmp = NULL;
			tmp++;
		}
		free(*tmp);
		free(s);
		s = NULL;
	}
}
