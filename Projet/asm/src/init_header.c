/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_header.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/03 16:03:25 by aparrot           #+#    #+#             */
/*   Updated: 2017/05/15 17:37:12 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"
#include "stdio.h"

static unsigned int		find_prog_size(t_core *lst)
{
	unsigned int	size;

	size = 0;
	while (lst)
	{
		size += lst->size;
		lst = lst->next;
	}
	return (size);
}

static void				strcpy_header(char *dest, char *src)
{
	char *ptr;

	ptr = ft_strchr(src, '"');
	ptr++;
	while (*ptr && *ptr != '"')
		*(dest++) = *(ptr++);
	*dest = 0;
}

t_header				init_header(t_core *lst, char **ali)
{
	unsigned int	prog_size;
	unsigned int	magic;
	int				i;
	t_header		header;

	magic = COREWAR_EXEC_MAGIC;
	i = 0;
	ft_bzero(&header, sizeof(t_header));
	while (is_emptyline_or_hashtagcomment(ali[i]))
		i++;
	header.magic = (unsigned int)endian(&magic, sizeof(magic));
	strcpy_header(header.prog_name, ali[i]);
	prog_size = find_prog_size(lst);
	header.prog_size = (unsigned int)endian(&prog_size, sizeof(unsigned int));
	strcpy_header(header.comment, ali[++i]);
	return (header);
}
